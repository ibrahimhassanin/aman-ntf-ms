package com.isoft.ntf.web.rest;

import com.isoft.ntf.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class EmailResource {

    private final Logger log = LoggerFactory.getLogger(EmailResource.class);

    @Autowired
    public EmailService emailService;

    @PostMapping("/send/{to}/subject/{subject}")
    public void sendMail(@PathVariable String to, @PathVariable String subject) {
        Map<String, Object> map = new HashMap<>();
        map.put("recipientName", "Ibrahim Mohamed");
        map.put("text", "Amr text!!!!");
        map.put("senderName", "Amr Ibrahim Mohamed");
//        map.put("dir","rtl");
        map.put("dir", "ltr");

        try {
            emailService.sendMessageUsingThymeleafTemplate(to, subject, map);
            log.debug("Email sent successfully to: [{}] with body: [{}]", to, subject);
//            log.debug("Email sent successfully!");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
