/**
 * View Models used by Spring MVC REST controllers.
 */
package com.isoft.ntf.web.rest.vm;
